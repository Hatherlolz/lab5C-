﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        class Loch
        {
            public string Name;
            public string Country;
            public string Region;
            public double Area;
            public double WaterVolume;
            public string Color;
            public bool HasWaterFall;

            public void CleanlinessOfTheLoch()
            {
                if (Country == "Україна" || Country == "Росія" || Country == "Білорусь")
                {
                    Console.WriteLine("Озеро брудне");
                }
                else
                    Console.Write("Озеро Чисте");

            }

        }
        static void Main(string[] args)
        {
            Console.Write("Введiть назву Озера: ");
            string sName = Console.ReadLine();

            Console.Write("Введiть назву країни где знаходиться озеро: ");
            string sCountry = Console.ReadLine();

            Console.Write("Введiть назву області где знаходиться озеро: ");
            string sRegion = Console.ReadLine();

            Console.Write("Введiть  площу бассейну: ");
            string sArea = Console.ReadLine();

            Console.Write("Введiть обсяг води: ");
            string sWaterVolume = Console.ReadLine();

            Console.Write("Введiть кольор води: ");
            string sColor = Console.ReadLine();


            Console.Write("Чи є біля озера водоспад? (у - так) (n - ні): ");
            ConsoleKeyInfo keyHasWaterFall = Console.ReadKey();
            Console.WriteLine();

            Loch OurLoch = new Loch();

            OurLoch.Name = sName;
            OurLoch.Country = sCountry;
            OurLoch.Region = sRegion;
            OurLoch.Area = double.Parse(sArea);
            OurLoch.WaterVolume = double.Parse(sWaterVolume);
            OurLoch.Color = sColor;
            OurLoch.HasWaterFall = keyHasWaterFall.Key == ConsoleKey.Y ? true : false;

            OurLoch.CleanlinessOfTheLoch();
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Данi про об`ект: ");
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Назва озера: " + OurLoch.Name);
            Console.WriteLine("Країна де знаходиться озеро: " + OurLoch.Country);
            Console.WriteLine("Область де знаходиться озеро: " + OurLoch.Region);
            Console.WriteLine("Площа бассейну: " + OurLoch.Area);
            Console.WriteLine("Обсяг води: " + OurLoch.WaterVolume);
            Console.WriteLine("Кольор води: " + OurLoch.Color);
            Console.WriteLine(OurLoch.HasWaterFall ? "Біля озера є водоспад" : "Біля озера немає водосподу");
            OurLoch.CleanlinessOfTheLoch();
            Console.ReadKey();

        }
    }
}